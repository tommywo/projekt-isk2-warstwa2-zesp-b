﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ComparePrzedmiotWykł
/// </summary>
public class ComparePrzedmiotWykł:IEqualityComparer<PrzedmiotWykladowca>
{
    public bool Equals(PrzedmiotWykladowca x, PrzedmiotWykladowca y)
    {
        if (x.idprzedmiotu == y.idprzedmiotu)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int GetHashCode(PrzedmiotWykladowca obj)
    {
        throw new NotImplementedException();
    }
}